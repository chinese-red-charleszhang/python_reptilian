import requests
from selenium import webdriver
from pyquery import PyQuery as pq

#获取歌名
def Access_to_the_song(C):
    doc=pq(C)
    The_song=doc('.audioName').text()
    return The_song

#进入听歌的界面爬取HTML
def Browser_access(URL):
    The_browser=webdriver.ChromeOptions()
    The_browser.set_headless()
    Drier=webdriver.Chrome(options=The_browser)
    Drier.get(URL)
    Print_source=Drier.page_source
    doc = pq(Print_source)
    Music_links = doc('.music').items()
    for i in Music_links:
        Music_downloads = i.attr('src')
        with open(Access_to_the_song(Print_source) + '.mp3', 'wb')as p:
            p.write(requests.get(Music_downloads).content)

#爬取热门歌曲

def Music_links():
    URL = 'http://www.kugou.com/yy/rank/home/1-6666.html?from=rank'
    headers={
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
    }
    response = requests.get(URL,headers=headers).text
    doc = pq(response)
    test = doc('.pc_temp_songname').items()
    for i in test:
        c = i.attr('href')
        Browser_access(c)

Music_links()
